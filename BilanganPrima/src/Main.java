import java.util.*;

public class Main {
    public static void main(String[] args) {
        int angka;
        String jawaban = "";
        Scanner keyboard = new Scanner(System.in);

        System.out.print("Masukkan angka: ");
        angka = keyboard.nextInt();

        int i = 2;
        if (angka < 2) {
            System.out.println("Bukan Bilangan Prima");
        }

        while (i < angka) {
            if (i == 2 || i==3) {
                jawaban = "Bilangan Prima";
            } else if (angka % i == 0) {
                jawaban = "Bukan Bilangan Prima";
                break;
            } else {
                jawaban = "Bilangan Prima";
            }
            i+=1;
        }
        System.out.println(jawaban);
    }
}